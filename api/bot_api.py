import numpy
import random
from bot import model, bag_of_words, labels, data, words


def ApiChat(text):
    results = model.predict([bag_of_words(text, words)])[0]
    print('result:')
    print(results)
    results_index = numpy.argmax(results)
    tag = labels[results_index]
    print('tag')
    print(tag)
        
    if results[results_index] >= 0.6:
        for tg in data["intents"]:
            if tg['tag'] == tag:
                responses = tg["responses"]
        return random.choice(responses)
    else:
        responses = "I didnt understand that.. try again!"
        return responses
