from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
import uvicorn
from api import bot_api

app = FastAPI()
templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/", response_class=HTMLResponse)
async def Home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/bot", response_class=HTMLResponse)
async def Home(request: Request):
    return templates.TemplateResponse("bot.html", {"request": request})


@app.post("/bot", response_class=HTMLResponse)
async def Home(request: Request, question: str=Form("question")):
    answer = bot_api.ApiChat(question)
    return templates.TemplateResponse("bot.html", {"request": request, "answer": answer})


if __name__ == '__main__':
    uvicorn.run(app)
