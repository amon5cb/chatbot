FROM tensorflow/tensorflow:latest

MAINTAINER "amon5cb@gmail.com"

WORKDIR /chatbot

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt  

ADD . /chatbot

CMD ["python3", "app.py"]